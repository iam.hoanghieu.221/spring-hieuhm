package org.digidinos.spring.config;

import org.digidinos.spring.lang.Language;
import org.digidinos.spring.lang.impl.Vietnamese;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ComponentScan({"org.digidinos.spring.bean"})
public class AppConfiguration {

    @Bean(name = "language")
    public Language getgLanguage(){
        return new Vietnamese();
    }
}
