package org.digidinos.spring.lang.impl;

import org.digidinos.spring.lang.Language;

public class Vietnamese implements Language {
    @Override
    public String getGreeting() {
        return "Xin Chao";
    }

    @Override
    public String getBye() {
        return "Tam Biet";
    }
}
