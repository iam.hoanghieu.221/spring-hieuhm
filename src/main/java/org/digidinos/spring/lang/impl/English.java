package org.digidinos.spring.lang.impl;

import org.digidinos.spring.lang.Language;

public class English implements Language {

    @Override
    public String getGreeting() {
        return "Hello";
    }

    @Override
    public String getBye() {
        return "Bye bye";
    }
}
