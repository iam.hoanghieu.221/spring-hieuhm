package org.digidinos.spring.lang;

public interface Language {
    public String getGreeting();

    public String getBye();
}
