package org.digidinos.spring;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.digidinos.spring.bean.GreetingService;
import org.digidinos.spring.bean.MyComponent;
import org.digidinos.spring.config.AppConfiguration;
import org.digidinos.spring.lang.Language;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainProgram {

    public static void main(String[] args) {
        //Tạo ra một đối tượng ApplicationContext bằng cách đọc cấu hình
        //trong class AppConfiguration
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);

        System.out.println("----------------------");
        Language language = (Language) context.getBean("language");

        System.out.println("Bean Language: " + language);
        System.out.println("Call language.sayBye(): " + language.getBye());

        System.out.println("-----------------------");

        GreetingService service = (GreetingService) context.getBean("greetingService");

        service.sayGreeting();

        MyComponent myComponent = (MyComponent) context.getBean("myComponent");

        myComponent.showAppInfo();

    }


}
